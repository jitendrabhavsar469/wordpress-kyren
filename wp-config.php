<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the installation.
 * You don't have to use the web site, you can copy this file to "wp-config.php"
 * and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * Database settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** Database settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'kyren' );

/** Database username */
define( 'DB_USER', 'root' );

/** Database password */
define( 'DB_PASSWORD', '' );

/** Database hostname */
define( 'DB_HOST', 'localhost' );

/** Database charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8mb4' );

/** The database collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication unique keys and salts.
 *
 * Change these to different unique phrases! You can generate these using
 * the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}.
 *
 * You can change these at any point in time to invalidate all existing cookies.
 * This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define( 'AUTH_KEY',         'IWu@aBZYaovdJTSrz{iRpxvQm6+R&3gIVOY.c.{>R3De|82hkEr|rBJ3|h=ASAqD' );
define( 'SECURE_AUTH_KEY',  'r7ekzE+6,&eFyu /!_4 42%dKnq<Nrw|ym)PKP9E@~K[]nikT*XrPm`&RE+:.ysX' );
define( 'LOGGED_IN_KEY',    'G2F K}4I%ESUcDJa7;~w6cQ?b4C+@;FO|Ddt9Yy-mLF}ddU(H6Z=H>`*.r88|snD' );
define( 'NONCE_KEY',        '}uC(i.$g}Dm|:>`^E</J@[3s(}sknc^E:!CA^G~j1j0yH*GPXal&e|:1w&Fd/<GJ' );
define( 'AUTH_SALT',        'p1K5?{kP7Hz/N,-v=|8K]@qQ)Awv<>30u~vJQMKHO@ei*Pl{uAeA(mMmVSD0-fw<' );
define( 'SECURE_AUTH_SALT', 'X1e19@rHnC3~.1^~Qa*%F}_tB-WG$4sWg(Sbi4xQs_(i(E Q@ -#xL!Q48<t F=v' );
define( 'LOGGED_IN_SALT',   'L.U*Y%aXyf*-.B~1@+)BWW_H,z7sVWEC3|Ln!-$i*(Xuz2>tn6yu*-@}`oS5tb9;' );
define( 'NONCE_SALT',       'V#Dg y9O^Mbo=LJc)#?k]bhC9`Xxn<307Dp h|> 1fZ@4:0q=4xp+p(*oDTC2u@j' );

/**#@-*/

/**
 * WordPress database table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'ke_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* Add any custom values between this line and the "stop editing" line. */



/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
